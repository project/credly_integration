<?php

namespace Drupal\credly_integration\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Psr\Http\Message\RequestInterface;
use \GuzzleHttp\Exception\RequestException;

/**
 * Provides a 'CredlyBadgeDisplayBlock' block.
 *
 * @Block(
 *  id = "credly_badge_display_block",
 *  admin_label = @Translation("Credly Badge Display Block"),
 * )
 */
class CredlyBadgeDisplayBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   */
    public function build()
    {
        $build = [];
        $build['credly_badge_display_block']['#markup'] = '<p>' . $this->credly_badges_content() . '</p>';
        return $build;
    }
    public function credly_badges_content()
    {
        $UserData = \Drupal::currentUser();
        $UserId = $UserData->id();
        $database = \Drupal::database();
        $HasUserInformation = $database->select('CredlyUserCredentialsInfo', 'n')
        ->fields('n')
        ->condition('uid', $UserId, '=')
        ->execute()
        ->fetchAssoc();
        $AccessToken = $HasUserInformation['AccessToken'];
        $client = \Drupal::httpClient();
        $AppApiSecret = \Drupal::config('system.site')->get('siteadmincredlysecret');
        $AppApiKey = \Drupal::config('system.site')->get('siteadmincredlyapikey');

        try {
            $response = $client->get('https://api.credly.com/v1.1/me/badges', [
              'query' => ['access_token' => $AccessToken],
              'headers' => [
              'X-Api-Key' => $AppApiKey,
              'X-Api-Secret' => $AppApiSecret
              ]]);
            $reponse_decode = json_decode($response->getBody()->getContents());
            $latest_badges = $reponse_decode->data;
             $str = "<a href='credly/content/user/info' target='_blank'><table><tr>";
             $str .= "Review earned badges, learn about other badge oportunities, add to LinkedIn and more...";
             $output = $str;
             //$badges_count = 0;
            foreach ($latest_badges as $key => $values) {
                  //if($badges_count <= 2){
                   $badges_title = $latest_badges[$key]->badge->title;
                   $badges_image_url = $latest_badges[$key]->badge->image_url;
                   $img = '<img src="'.$badges_image_url.'" alt="'.$badges_title.'"  height="100" width="100">';
                   $output .="<td>" .$img."<br><strong>".$badges_title."</strong></td>" ;
                   $badges_count++;
                 //}
            }
             $output .= "</tr></table></a>";
            return $output;
        } catch (RequestException $exception) {
            $output = "<table><tr>No Credly Badges For You</tr></table>";
            return $output;
        }
        //return $BlockBody;
    }
}
